/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('college', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            field: 'id',
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: true,
            field: 'name'
        },

    }, {
        tableName: 'college'
    },{ timestamps: false });
};
