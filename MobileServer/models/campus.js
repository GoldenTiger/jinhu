/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('campus', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            field: 'id',
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: true,
            field: 'name'
        },
        salenum: {
            type: DataTypes.INTEGER(6),
            allowNull: false,
            default: 0,
            field: 'salenum'
        },

    }, {
        tableName: 'campus'
    },{ timestamps: false });
};
