/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('user', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            field: 'id',
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(20),
            allowNull: false,
            field: 'name'
        },
        gender: {
            type: DataTypes.INTEGER(2),
            allowNull: true,
            field: 'gender'
        },
        role: {
            type: DataTypes.STRING(50),
            allowNull: false,
            field: 'role'
        },
        phone: {
            type: DataTypes.STRING(255),
            allowNull: false,
            field: 'phone'
        },
        salenum: {
            type: DataTypes.INTEGER(5),
            allowNull: true,
            default:0,
            field: 'salenum'
        },
        campusID: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'campus',
                key: 'id'
            }
        },
        teamID: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'team',
                key: 'id'
            }
        },
        collegeID: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'college',
                key: 'id'
            }
        },
    }, {
        tableName: 'user'
    },{ timestamps: false });
};
