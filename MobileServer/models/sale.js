/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('sale', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            field: 'id',
            autoIncrement: true
        },
        phonenum: {
            type: DataTypes.STRING(50),
            allowNull: false,
            field: 'phonenum'
        },
        saler: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'user',
                key: 'id'
            },
            field: 'saler'
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'createdAt'
        },

    }, {
        tableName: 'sale'

    },{ timestamps: false });
};
