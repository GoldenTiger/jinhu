/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('team', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            field: 'id',
            autoIncrement: true
        },
        teamame: {
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: true,
            field: 'teamname'
        },
        salenum: {
            type: DataTypes.INTEGER(6),
            allowNull: false,
            default: 0,
            field: 'salenum'
        },
        teamleader: {
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: true,
            field: 'teamleader'
        },
    }, {
        tableName: 'team'
    },{ timestamps: false });
};
