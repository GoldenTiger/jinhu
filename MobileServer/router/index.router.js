const express = require('express');
const router = express.Router();


const ctrlUser = require('../controllers/user.controller');
const ctrSale=require('../controllers/sale.controller')


router.post('/login', ctrlUser.login);
router.post('/register', ctrlUser.register);
router.post('/addnewsale',ctrSale.insertNewReacord)
router.get('/getsalenum/:id',ctrSale.getsalenum)
router.get('/getteamsales/:id',ctrSale.getTeamSales)
router.get('/getAllCampusSales',ctrSale.getAllCampusSales)
router.get('/getAllTeamSales',ctrSale.getAllTeamSales)
module.exports = router;
