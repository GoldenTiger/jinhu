import Sequelize from 'sequelize';

import TeamModel from './models/team';
import SaleModel from './models/sale';
import CampusModel from './models/campus'
import UserModel from './models/user';

const sequelize = new Sequelize('mobile', 'root', '83883758', {
    // const sequelize = new Sequelize('eps', 'nodeuser', 'password', {
    host: '129.28.3.154',
    dialect: 'mysql',
    operatorsAliases: false,
});

const Team = TeamModel(sequelize, Sequelize);
const Sale = SaleModel(sequelize, Sequelize);
const Campus =CampusModel(sequelize,Sequelize);
const User= UserModel(sequelize,Sequelize);



sequelize.sync().then(() => {
    console.log(`EPS db and user table have been created`);
});

module.exports = {
    Team,
    Sale,
    Campus,
    User,
    sequelize
};
