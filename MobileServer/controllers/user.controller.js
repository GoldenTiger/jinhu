import SqlModel from '../sequelize';

const _ = require('lodash');

const sqlUserModel = SqlModel.User;



module.exports.login = (req, res, next) =>{
    console.log('getUserProfile');
    console.log('getUserProfile req');
    console.log('req.id:' + req.body.phone);
    console.log('getUserProfile res'+req.body.name);
    // console.log(res);
    sqlUserModel.findOne({
        where: {
            phone: req.body.phone,
        },
    })
        .then(user => {
            if (!user)
                return res.status(200).json({ status: false, message: '手机号未录入！' });
            else {
                if (user.name==req.body.name) {
                    return res.status(200).json({ status: true, user : _.pick(user,['id','role','name','gender','teamID','campusID'])});
                }else {
                    return res.status(200).json({ status: false, message: '姓名号码不匹配！' });
                }

            }

        });
};


module.exports.register = (req, res, next) =>{
    console.log('setUserProfile');
    console.log('req.id:' + req.body.id);
    // console.log(res);
    const data = {
        username: req.body.name,
        gender: req.body.gender,
        collegeID: req.body.college,
        role: 'student',
        phone: req.body.phone,
        teamID: req.body.team,
        campusID: req.body.campus,
        id: req.body.id
    };
    console.log(data)
    sqlUserModel.update({
        campusID: data.campusID,
        name: data.username,
        gender: data.gender,
        teamID: data.teamID,
        collegeID: data.collegeID,
        role: data.role,
        phone: data.phone,
    }, { where: {id: data.id}
    }).then(user => {
            console.log('user created in db');
            res.status(200).send({message: 'user created'});
        }).catch(err=>{
            console.log('dataBase Error:')
            console.log(err)
    });
};
