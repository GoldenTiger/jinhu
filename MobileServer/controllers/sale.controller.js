import SqlModel from '../sequelize';

const _ = require('lodash');

const saleModel = SqlModel.Sale;
const UserModel=SqlModel.User;
const CampusModel=SqlModel.Campus;
const TeamModel=SqlModel.Team;

// Parameter : UserID, Note,
module.exports.insertNewReacord = (req, res, next) => {
    console.log('insert new record');
    const data = {
        phone: req.body.phone,
        note: req.body.note,
        saler: req.body.saler,
        team: req.body.teamID,
        campus: req.body.campusID,
    };
    console.log(data);
    saleModel.create({
        phonenum: data.phone,
        note: data.note,
        saler: data.saler,
        createAt: new Date(),
    }).then(newSale => {
            UserModel.findById(data.saler).then(function (user) {
                user.increment('salenum').then(function (result) {
                    CampusModel.findById(data.campus).then(function (campus) {
                        campus.increment('salenum');
                    })
                    TeamModel.findById(data.team).then(function (team) {
                        team.increment('salenum');
                    })
                    res.status(200).send({status: true,message: 'success'});
                }).catch(err=>{
                    res.status(200).send({status: true,message: err});
                 });
            })
        }).catch(err=>{
        console.log('dataBase Error:')
        res.status(200).send({status: true,message: err});
    });
};

// Parameter : UserID
module.exports.getsalenum = (req, res, next) =>{
    console.log('Get Personal Sale Number:'+req.params.id);
    const UserID=req.params.id
    saleModel.sequelize.query("select * from sale where saler="+"'"+UserID+"'")
        .then(user => {
            console.log(user)
                res.setHeader('Content-Type', 'text/plain');
                return res.status(200).json({ status: true, user : user[0]});
        });
};

// Parameter : TeamID
module.exports.getTeamSales = (req,res,next) =>{
    console.log('TeamID'+req.params.id);
    const TeamID=req.params.id
    if (TeamID==null){
        return res.status(200).json({ status: false, message:'用户登陆过期！'});
    } else {
        saleModel.sequelize.query("select name,salenum from user where teamID="+"'"+TeamID+"'")
            .then(TeamSale => {
                console.log(TeamSale)
                return res.status(200).json({ status: true, TeamSale : TeamSale[0]});
            });
    }

};


module.exports.getAllTeamSales = (req,res,next) =>{
    saleModel.sequelize.query("select teamname,salenum from team")
        .then(TeamSale => {
            console.log(TeamSale)
            return res.status(200).json({ status: true, TeamSale : TeamSale[0]});
        });
}




module.exports.getAllCampusSales = (req,res,next) =>{
    saleModel.sequelize.query("select * from campus")
        .then(TeamSale => {
            console.log(TeamSale)
            return res.status(200).json({ status: true, TeamSale : TeamSale[0]});
        });
}
