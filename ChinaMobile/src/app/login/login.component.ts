import { Component, OnInit } from '@angular/core';
import {User, UserService} from '../shared/user.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import { SessionService } from '../shared/session.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private userRole: string;
  serverErrorMessage: string;
  res: {status: number, user: User};
  constructor(private userService: UserService,
              private router: Router,
              private sessionService: SessionService) { }
  ngOnInit() {
  }

  onSubmit(value: any) {
    sessionStorage.clear();
    if (value.name === '' || value.phone === '') {
      Swal.fire({
        title: '错误!',
        text: '输入框不能为空',
        type: 'error',
        confirmButtonText: '确认'
      });
      // swal('错误', '字段不能为空', 'error');
    } else {
      console.log(value);
      this.userService.Login(value).subscribe(
        res => {
          console.log(res['status']);
          if (res['status']) {
            console.log(res['user']);
            this.sessionService.setSession(res)
            this.userService.campusID = res['user'].campusID;
            this.userService.teamID=res['user'].teamID;
            if (res['user'].teamID==null){
              this.router.navigateByUrl('/profile');
            }else{
              if (res['user'].role === ('student'))  {
                this.router.navigateByUrl('/userindex');
              } else {
                this.router.navigateByUrl('/saleinfo');
              }
            }

          } else {
            Swal.fire({
              title: '登陆失败!',
              text: res['message'],
              type: 'error',
              confirmButtonText: '确认'
            });
          }
        },
        err => {
          this.serverErrorMessage = err.error.message;
        });
    }

  }
}


