import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import {IndexComponent} from './userpage/index/index.component';
import {LoginComponent} from './login/login.component';
import {ProfileComponent} from './userpage/profile/profile.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ManageindexComponent} from './managepage/manageindex/manageindex.component';
import {SaleinfoComponent} from './managepage/saleinfo/saleinfo.component';
import {LoginGuard} from './guard/login.guard';
import {UserService} from './shared/user.service';
import {RoleGuard} from './guard/role.guard';




const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'userindex', component: IndexComponent, canActivate: [LoginGuard]},
  {path: 'profile', component: ProfileComponent, canActivate: [LoginGuard]},
  {path: 'dashboard', component: DashboardComponent, canActivate: [RoleGuard, LoginGuard]},
  {path: 'saleinfo', component: SaleinfoComponent, canActivate: [RoleGuard, LoginGuard]},
  {path: 'manageindex', component: ManageindexComponent, canActivate: [RoleGuard, LoginGuard]},
  {path: '**', component: LoginComponent}
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  providers: [UserService, LoginGuard]
})
export class AppRoutingModule {
}
