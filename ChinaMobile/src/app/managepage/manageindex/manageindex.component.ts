import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import Swal from 'sweetalert2';
import {UserService} from '../../shared/user.service';
import {EmitService} from '../../shared/emit.service';
import {Sale, SaleService} from '../../shared/sale.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-manageindex',
  templateUrl: './manageindex.component.html',
  styleUrls: ['./manageindex.component.css']
})
export class ManageindexComponent implements OnInit {
  dataSource: Observable<any>;
  sales: Array<Sale> = [];
  salenum: number;
  phoneRegex = /^1[3-9](\d{9})$/;
  constructor(private userService: UserService,
              public emitService: EmitService,
              private saleService: SaleService) {
    this.dataSource = this.saleService.getsalenum();
  }

  ngOnInit() {
      this.emitService.evenEmit.subscribe((value: any) => {
        // tslint:disable-next-line:triple-equals
        if ( value == 'addphone') {
          // 这里就可以调取接口，刷新userList列表数据
          this.dataSource.subscribe(
            (data) => {
              console.log(data);
              this.salenum = data.user.length;
              this.sales = data.user;
            }
          );
        }
      });
      this.dataSource.subscribe(
        (data) => {
          console.log(data);
          this.salenum = data.user.length;
          this.sales = data.user;
        }
      );
    }

  onSubmit(form: NgForm) {
    const value = form.value;
    value.saler = this.userService.user;
    console.log(value);
    this.saleService.addnewsale(value).subscribe(res => {
      const message = res['message'];
      console.log(message);
      if (message === 'success') {
        Swal.fire({
          title: '提示!',
          text: '添加成功',
          type: 'success',
          confirmButtonText: '确认'
        });
        this.emitService.evenEmit.emit('addphone');
        form.resetForm();
      } else {
        Swal.fire({
          title: '提示!',
          text: '添加失败',
          type: 'error',
          confirmButtonText: '确认'
        });
      }
    });
  }
}
