import { Component, OnInit, OnDestroy } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SaleService} from '../../shared/sale.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-saleinfo',
  templateUrl: './saleinfo.component.html',
  styleUrls: ['./saleinfo.component.css']
})
export class SaleinfoComponent implements OnInit {
  private datasource: Observable<any>;
  private TeamList: Array<any>;
  private timer;
  private TeamScore;
  constructor(private http: HttpClient,
              private saleService: SaleService,
              private router: Router,
              private userService: UserService,
              ) {
      this.datasource = this.saleService.getTeamSale();
      this.timer = setInterval(() => {
        console.log("Seesion-TeamID:" + sessionStorage.getItem("TeamID"))
        if (this.userService.teamID!=null) {
          this.datasource.subscribe(data => {
            this.TeamList = data['TeamSale'];
            this.countTotalNum(this.TeamList);
          });
        } else {
          Swal.fire({
            title: '错误',
            text: '用户登陆信息过期',
            type: 'error',
            confirmButtonText: '确认'
          });
          this.router.navigateByUrl('/login');
        }
      }, 5000);
  }



  ngOnDestroy() {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  ngOnInit(): void {
    this.datasource.subscribe(data => {
      this.TeamList = data['TeamSale'];
      console.log(data);
    });
  }

  countTotalNum(saleList){
    let countTotalNum=0
    saleList.forEach(element => {
      countTotalNum = countTotalNum + element.salenum;
    });
    this.TeamScore=countTotalNum;
  }
}
