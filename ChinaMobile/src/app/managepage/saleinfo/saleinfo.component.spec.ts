import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleinfoComponent } from './saleinfo.component';

describe('SaleinfoComponent', () => {
  let component: SaleinfoComponent;
  let fixture: ComponentFixture<SaleinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
