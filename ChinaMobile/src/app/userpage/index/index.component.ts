import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {from, Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import {Sale, SaleService} from '../../shared/sale.service';
import {UserService} from '../../shared/user.service';
import {Data, Router} from '@angular/router';
import {EmitService} from '../../shared/emit.service';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  dataSource: Observable<any>;
  sales: Array<Sale> = [];
  salenum: number;
  constructor(private httpclient: HttpClient,
              private userService: UserService,
              private saleService: SaleService,
              private emitService: EmitService) {
    this.dataSource = this.saleService.getsalenum();
  }

  ngOnInit() {
    this.emitService.evenEmit.subscribe((value: any) => {
      // tslint:disable-next-line:triple-equals
      if ( value == 'addphone') {
        // 这里就可以调取接口，刷新userList列表数据
        this.dataSource.subscribe(
          (data) => {
            console.log(data)
            this.salenum = data.user.length;
            this.sales = data.user;
          }
        );
      }
    });
    this.dataSource.subscribe(
      (data) => {
        console.log(data)
        this.salenum = data.user.length;
        this.sales = data.user;
      }
    );
  }
}


