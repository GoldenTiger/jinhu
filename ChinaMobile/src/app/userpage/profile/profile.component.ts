import { Component, OnInit } from '@angular/core';
import {User, UserService} from '../../shared/user.service';
import {Router} from '@angular/router';
import { NgForm} from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: User;
  userid: number;
  constructor(private userService: UserService,
              private router: Router) { }

  ngOnInit() {
    this.userid = parseInt(sessionStorage.getItem('userID'), 10);
    console.log('userid' + sessionStorage.getItem('userID'));
    this.user = this.userService.getUser();
  }

  onSubmit(form: NgForm) {
    const value = form.value;
    value.id = this.userid;
    console.log(value);
    this.userService.register(value).subscribe(
      res => {
        Swal.fire({
          title: '提示!',
          text: '更新成功',
          type: 'success',
          confirmButtonText: '确认'
        });
        console.log('success:' + res);
      }, err => {
        console.log(err);
      }
    );
  }

}
