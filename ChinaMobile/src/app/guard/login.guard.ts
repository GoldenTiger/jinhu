import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {UserService} from '../shared/user.service';
import {Injectable} from '@angular/core';

@Injectable()
export class LoginGuard implements CanActivate {

    constructor(private userService: UserService,
                private router: Router) {
    }
    canActivate() {
      console.log('守卫！');
      if (!this.userService.isLoginIn()) {
        console.log('用户未登陆！');
        this.router.navigate(['login']);
      } else {
        return true;
      }

  }
}

