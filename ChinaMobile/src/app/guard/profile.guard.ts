import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {UserService} from '../shared/user.service';
import {Injectable} from '@angular/core';

@Injectable()
export class RoleGuard implements CanActivate {

  constructor(private userService: UserService,
              private router: Router) {
  }
  canActivate() {
    console.log('身份守卫！');
    if (this.userService.isManager()) {
      this.router.navigateByUrl('/userindex');
    } else {
      return true;
    }
  }
}

