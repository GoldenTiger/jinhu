import {EventEmitter, Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmitService {
  public evenEmit: any;
  constructor() {
    this.evenEmit = new EventEmitter();
  }
}
