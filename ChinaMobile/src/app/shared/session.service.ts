import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './user.service';


@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private router: Router,
    private userService: UserService) { }
    public setSession(response){
      console.log(response);
      sessionStorage.setItem('userID', response['user'].id);
      sessionStorage.setItem('userRole', response['user'].role);
      sessionStorage.setItem('campusID',response['user'].campusID);
      // sessionStorage.setItem('teamID',response['user'].teamID);
      if (response['user'].teamID==null){
          // this.route.
          console.log('sdasdasdas')
        this.router.navigateByUrl('/profile');
      }else{
        sessionStorage.setItem('teamID', response['user'].teamID);
      }
      
  }
}
