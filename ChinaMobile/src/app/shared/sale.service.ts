import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserService} from './user.service';
import {NgForm} from '@angular/forms';
import {Data} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SaleService {

  constructor(private http: HttpClient,
              private userService: UserService) { }

  addnewsale(formvalue) {
    return this.http.post('api/addnewsale', formvalue);
  }

  getsalenum() {
    return this.http.get('api/getsalenum/' + this.userService.user);
  }

  getTeamSale() {
    const ID = this.userService.teamID;
    return this.http.get('api/getteamsales/' + ID);
  }

}


export class Sale {
  constructor(public id: number,
              public phonenum: string,
              public createdAt: Data) {}
}
