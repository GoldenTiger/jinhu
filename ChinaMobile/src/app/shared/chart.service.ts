import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  constructor(private http: HttpClient) { }

  getCampusSale() {
    return this.http.get('/api/getAllCampusSales');
  }

  getTeamSales() {
    return this.http.get('/api/getAllTeamSales');
  }
}
