import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public user;
  public teamID;
  public campusID;
  constructor(private http: HttpClient) { }

  getUser(): User {
    return new User(1, '汪金虎', '18852993720', 1, 1, 1, 'Computer Scicence');
  }

  register(registerForm) {
    return this.http.post('api/register', registerForm);
  }

  Login(loginFormcontent) {
    return this.http.post('/api/login', loginFormcontent);
  }

  isManager() {
    // tslint:disable-next-line:triple-equals
    if (sessionStorage.getItem('userRole') == 'manage') {
      return true;
    }
  }

  isLoginIn() {
    this.user = sessionStorage.getItem('userID')
    if (this.user) {
      return true;
    }
  }
}


export class User {
  constructor(
    public id: number,
    public name: string,
    public phone: string,
    public gender: number,
    public campus: number,
    public college: number,
    public major: string
  ) {

  }
}
