import { Component, OnInit } from '@angular/core';
import {SaleService} from '../../shared/sale.service';
import {ChartService} from '../../shared/chart.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {

  public pieChartLabels = ['东南', '东湖'];
  public pieChartData = [0, 0];
  public pieChartType = 'pie';
  private dataSource: Observable<any>;
  private CampusList: Array<any>;
  constructor(private chartService: ChartService) {
    this.dataSource = chartService.getCampusSale();
  }
  ngOnInit() {
    this.dataSource.subscribe(data => {
      this.CampusList = data.TeamSale;
      const data1 = this.CampusList[0].salenum;
      console.log(data1);
      this.pieChartData = [data1, data1];
    });
  }

}
