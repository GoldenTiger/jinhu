import { Component, OnInit } from '@angular/core';
import { ChartService } from 'src/app/shared/chart.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {

  constructor(private chartService: ChartService) { 
    this.dataSource=chartService.getTeamSales();
  }
  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels = [];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    {data: [], label: '东南'},
    { data: [], label: '东湖'},
  ];
  private dataSource: Observable<any>;
  private TeamList: Array<any>;

  ngOnInit() {
    this.dataSource.subscribe(data => {
      this.TeamList=data.TeamSale;
      this.TeamList.forEach(element => {
        if(element.campus=='东南'){
          this.barChartLabels.push(element.teamname)
          this.barChartData[0].data.push(element.salenum)
        }else{
          this.barChartLabels.push(element.teamname)
          this.barChartData[1].data.push(element.salenum)
        }
      });
    });
  }
}
