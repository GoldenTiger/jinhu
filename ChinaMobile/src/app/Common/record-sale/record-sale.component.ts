import { Component, OnInit } from '@angular/core';
import {UserService} from '../../shared/user.service';
import {Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, NgForm, NgModel, Validators} from '@angular/forms';
import {SaleService} from '../../shared/sale.service';
import Swal from 'sweetalert2';
import {EmitService} from '../../shared/emit.service';

@Component({
  selector: 'app-record-sale',
  templateUrl: './record-sale.component.html',
  styleUrls: ['./record-sale.component.css']
})
export class RecordSaleComponent implements OnInit {

  phoneRegex = /^1[3-9](\d{9})$/;
  constructor(private userService: UserService,
              private router: Router,
              private saleService: SaleService,
              public emitService: EmitService
  ) { }
  ngOnInit() {
    if (!this.userService.isLoginIn()) {
      this.router.navigateByUrl('/login');
    }
  }

  onSubmit(form: NgForm) {
    const value = form.value
    value['saler'] = this.userService.user;
    value['teamID'] = this.userService.teamID;
    value['campusID'] = this.userService.campusID;
    console.log(value);
    this.saleService.addnewsale(value).subscribe(res => {
      const message = res['message'];
      console.log(message)
      if (message === 'success') {
        Swal.fire({
          title: '提示!',
          text: '添加成功',
          type: 'success',
          confirmButtonText: '确认'
        });
        this.emitService.evenEmit.emit('addphone');
        form.resetForm();
      } else {
        Swal.fire({
          title: '提示!',
          text: '添加失败',
          type: 'error',
          confirmButtonText: '确认'
        });
      }
    });
  }

}

