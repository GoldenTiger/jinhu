import { BrowserModule } from '@angular/platform-browser';
import {Inject, NgModule} from '@angular/core';


import { AppComponent } from './app.component';
import { ProfileComponent } from './userpage/profile/profile.component';
import { IndexComponent } from './userpage/index/index.component';
import { SaleinfoComponent } from './managepage/saleinfo/saleinfo.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { AppRoutingModule } from './app-routing.module';
import {RouterModule} from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';
import { ManageindexComponent } from './managepage/manageindex/manageindex.component';
import { MnavbarComponent } from './managepage/mnavbar/mnavbar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UserService} from './shared/user.service';
import {HttpClientModule} from '@angular/common/http';
import {AngularWebStorageModule} from 'angular-web-storage';
import {environment} from '../environments/environment.prod';
import {EmitService} from './shared/emit.service';
import {RoleGuard} from './guard/role.guard';
import {LoginGuard} from './guard/login.guard';
import { RecordSaleComponent } from './Common/record-sale/record-sale.component';
import {ChartsModule} from 'ng2-charts';
import { BarChartComponent } from './chart/bar-chart/bar-chart.component';
import { PieChartComponent } from './chart/pie-chart/pie-chart.component';
import { SessionService } from './shared/session.service';


@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    IndexComponent,
    SaleinfoComponent,
    LoginComponent,
    DashboardComponent,
    NavbarComponent,
    FooterComponent,
    ManageindexComponent,
    MnavbarComponent,
    RecordSaleComponent,
    BarChartComponent,
    PieChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ReactiveFormsModule,
    AngularWebStorageModule,
    ChartsModule
  ],
  providers: [{provide: APP_BASE_HREF, useValue : '/' }, UserService,SessionService,
    {provide: 'BACKEND_API_URL', useValue: environment.backendApiUrl},
    {provide: 'DEFAULT_LANGUAGE', useValue: environment.defaultLanguage}, EmitService, RoleGuard, LoginGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(@Inject('BACKEND_API_URL') private apiUrl: string) {}
}
